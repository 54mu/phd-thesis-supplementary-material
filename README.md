---
title: PhD thesis supplementary material repository
author: Samuele Greco
---


# PhD thesis supplementary material

A repository that contains the supplementary material related to my PhD thesis

# File descriptions

## *Trematomus bernacchii*

The [directory](Trematomus_bernacchii) contains:
+ Differential Expression Analysis results
    + heat effect
        + brain
        + gills
    + stabling effect
        + brain
        + gills
+ Gene Ontology enrichment analysis results
    + heat effect
        + brain
        + gills
    + stabling effect
        + brain
        + gill
+ Read mapping data
    + one sub-folder per sample, containing the output of each mapping run
+ Read mapping results
    + merged quantifications in counts and tpm
+ Read trimming reports
    + one file per sample, the outputs of the trimming runs

## *Adamussium colbecki*

The [main directory](Adamussium_colbecki) contains:

The subdirectories are structured as follows:
+ Differential Expression analysis results
    + The results of the test for the statistically significant DEGs, i.e. the results of the stabling stress on digestive gland
+ Read mapping data
    + One subdirectory per sample, containing the output of each mapping run
+ Read mapping results
    + Merged quantifications in counts and tpm
+ Read trimming reportd
    + One file per sample, the outputs of the trimming runs
+ Reference transcriptome
    + The assembled filtered reference transcriptome in fasta format
    + The annotation for the transcriptome in tsv format
    + The map of each transcript to the respective putative genes
+ Metadata of samples

## *Pseudorchomene* sp.

The [main directory](Pseudorchomene_sp) constianis:
+ Read trimming reports
    + One file per sample
+ transcriptome
    + the assembled transcriptome
    + list of sequence IDs only classifiable as *Hematodinium*
    + list of sequence IDs only classifiable as *Pseudorchomene*
+ phylo trees
    + raw tree file for COX1
    + raw tree file for 28S
+ concatenated common busco alignment
    + superalignment used for genetic divergence among samples
+ separate_samples_assemblies
    + One file per sample, Trinity assembly of transcriptome for each individual sample.

## *Acutuncus antarcticus*
The [main directory](Acutuncus_antarcticus) contains:
+  differential expression analysis results (Only results for significant DEGs are reported)
    + DEGs test results for Long Term Heat expousure 
    + DEGs test results forn Short Term Heat exposure
+ enrichment analysis results
    + Gene Ontology Enrichment analysis results for the genes up-regulated with temperature in short term exposure experiment
+ read mapping data 
    + One subdirectory per sample, containing the output of each mapping run
+ read mapping results
    + merged quantifications of gene expression in counts and tpm
+ read trimming reports
    + two files per sample, quality control data for trimmed reads
+ reference transcriptome
    + Assembled transcriptome in fasta format
    + transcriptome annotations in tabular format
    + transcript id to putativ gene (by similarity) map


# Citation

If you use data or code from this repository please cite the web link to here or:

```
to be determined, if you really are in a hurry open an issue and tell me
```
